const webSocket = require('ws');
const PORT = 7000;

const wsServer = new webSocket.Server({
    port: PORT
});
wsServer.on('connection', function (socket) {
    //some feedback on the console
    console.log("A client just connected:");

    //Attach some behavior to the incoming socket
    socket.on('message', function (msg) {
        console.log("Received message from client:" + msg);
        // socket.send("take this back:" + msg)
        //broadcast that messages to all connected clients
        wsServer.clients.forEach(function (client) {
            client.send("someone said:" + msg);
        });
    });

});

console.log((new Date()) + "Servers is listening:" + PORT);